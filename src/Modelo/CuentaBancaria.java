
package Modelo;

public class CuentaBancaria {
    private int numCuenta;
    private String fechaApertura,nomBanc;
    private float porcenRendi,saldo;
    private Cliente cli;

    public CuentaBancaria() {
        numCuenta = 0;
        fechaApertura = " ";
        nomBanc = " ";
        porcenRendi = 0.0f;
        saldo = 0.0f;
        cli = null;
    }

    public CuentaBancaria(int numCuenta, String fechaApertura, String nomBanc, float porcenRendi, float saldo, Cliente cli) {
        this.numCuenta = numCuenta;
        this.fechaApertura = fechaApertura;
        this.nomBanc = nomBanc;
        this.porcenRendi = porcenRendi;
        this.saldo = saldo;
        this.cli = cli;
    }
    
    public CuentaBancaria(CuentaBancaria cuentBanc) {
        this.numCuenta = cuentBanc.numCuenta;
        this.fechaApertura = cuentBanc.fechaApertura;
        this.nomBanc = cuentBanc.nomBanc;
        this.porcenRendi = cuentBanc.porcenRendi;
        this.saldo = cuentBanc.saldo;
        this.cli = cuentBanc.cli;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public void setNomBanc(String nomBanc) {
        this.nomBanc = nomBanc;
    }

    public void setPorcenRendi(float porcenRendi) {
        this.porcenRendi = porcenRendi;
    }
    
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void setCli(Cliente cli) {
        this.cli = cli;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public String getNomBanc() {
        return nomBanc;
    }

    public float getPorcenRendi() {
        return porcenRendi;
    }

    public float getSaldo() {
        return saldo;
    }

    public Cliente getCli() {
        return cli;
    }
    
    public void depositar(float cant){
        setSaldo(cant+=getSaldo());
    }
    
    public boolean retirar(float cant){
        float sald = getSaldo();
        if(cant <= getSaldo()){
            setSaldo(sald-=cant);
            return true;
        }
        else{
            return false;
        }
    }
    
    public float calcularReditos(){
        return getPorcenRendi()*(getSaldo()/365f);
    }
}
