package Modelo;

public class Cliente {
    private String nomCli,fechaNaci,dom,sex;

    public Cliente() {
        nomCli=" ";
        fechaNaci=" ";
        dom=" ";
        sex=" ";
    }

    public Cliente(String nomCli, String fechaNaci, String dom, String sex) {
        this.nomCli = nomCli;
        this.fechaNaci = fechaNaci;
        this.dom = dom;
        this.sex = sex;
    }
    
    public Cliente(Cliente cli) {
        this.nomCli = cli.nomCli;
        this.fechaNaci = cli.fechaNaci;
        this.dom = cli.dom;
        this.sex = cli.sex;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public void setFechaNaci(String fechaNaci) {
        this.fechaNaci = fechaNaci;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNomCli() {
        return nomCli;
    }

    public String getFechaNaci() {
        return fechaNaci;
    }

    public String getDom() {
        return dom;
    }

    public String getSex() {
        return sex;
    }
    
    
}