package Controlador;

import Modelo.CuentaBancaria;
import Modelo.Cliente;
import Vista.dlgBanco;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private CuentaBancaria banc;
    private dlgBanco vista;

    public Controlador(CuentaBancaria banc, dlgBanco vista) {
        this.banc = banc;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        
        vista.radBtnMasculino.addActionListener(this);
        vista.radBtnFemenino.addActionListener(this);
        
        vista.btnDeposito.addActionListener(this);
        vista.btnRetiro.addActionListener(this); 
    }
    
    private void iniciarVista(){
        vista.setTitle(":: BANCO ::");
        vista.setSize(939, 675);
        vista.setVisible(true);

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo){
            vista.txtDom.setEnabled(true);
            vista.txtFechAper.setEnabled(true);
            vista.txtFechNac.setEnabled(true);
            vista.txtNomBanc.setEnabled(true);
            vista.txtNomCli.setEnabled(true);
            vista.txtNumCuenta.setEnabled(true);
            vista.txtPorcenRendi.setEnabled(true);
            vista.txtSaldo.setEnabled(true);
            
            
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
           
            
            vista.radBtnFemenino.setEnabled(true);
            vista.radBtnMasculino.setEnabled(true);
        }
        else if(e.getSource() == vista.btnGuardar){
            if(ValiGuard() == true){
                try{
                    banc.setNumCuenta(Integer.parseInt(vista.txtNumCuenta.getText()));
                    banc.setNomBanc(vista.txtNomBanc.getText());
                    banc.setFechaApertura(vista.txtFechAper.getText());
                    banc.setPorcenRendi(Float.parseFloat(vista.txtPorcenRendi.getText()));
                    banc.setSaldo(Float.parseFloat(vista.txtSaldo.getText()));
                    banc.setCli(new Cliente(vista.txtNomCli.getText(),vista.txtFechNac.getText(),vista.txtDom.getText(),sex()));
                    vista.txtDom.setEnabled(false);
                    vista.txtFechAper.setEnabled(false);
                    vista.txtFechNac.setEnabled(false);
                    vista.txtNomBanc.setEnabled(false);
                    vista.txtNomCli.setEnabled(false);
                    vista.txtNumCuenta.setEnabled(false);
                    vista.txtPorcenRendi.setEnabled(false);
                    vista.txtSaldo.setEnabled(false);
                    vista.radBtnFemenino.setEnabled(false);
                    vista.radBtnMasculino.setEnabled(false);
                    vista.txtCant.setEnabled(true);
                    vista.txtNuevSaldo.setEnabled(true);
                    vista.btnDeposito.setEnabled(true);
                    vista.btnRetiro.setEnabled(true);
                    limpiar();
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                    
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }
                
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }
            
        }
        else if(e.getSource() == vista.btnMostrar){
            vista.txtNumCuenta.setText(String.valueOf(banc.getNumCuenta()));
            vista.txtNomCli.setText(banc.getCli().getNomCli());
            vista.txtDom.setText(banc.getCli().getDom());
            vista.txtFechNac.setText(banc.getCli().getFechaNaci());
            sexG();
            vista.txtNomBanc.setText(banc.getNomBanc());
            vista.txtFechAper.setText(banc.getFechaApertura());
            vista.txtPorcenRendi.setText(String.valueOf(banc.getPorcenRendi()));
            vista.txtSaldo.setText(String.valueOf(banc.getSaldo()));
        }
        else if(e.getSource() == vista.btnDeposito){
            if(ValiCant()==true){
                try{
                    banc.depositar(Float.parseFloat(vista.txtCant.getText()));
                    vista.txtNuevSaldo.setText(String.valueOf(banc.getSaldo()));
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }
                
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }         
        }
        else if(e.getSource() == vista.btnRetiro){
            if(ValiCant()==true){
                try{
                    if(banc.retirar(Float.parseFloat(vista.txtCant.getText()))==true){
                    vista.txtNuevSaldo.setText(String.valueOf(banc.getSaldo()));
                    }
                    else{
                        JOptionPane.showMessageDialog(vista,"No tiene suficiente dinero para retirar esa cantidad");
                    }
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }
                 
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }
        }
    }
    
    public void limpiar(){
        vista.txtNumCuenta.setText(" ");
        vista.txtNomCli.setText(" ");
        vista.txtDom.setText(" ");
        vista.txtFechNac.setText(" ");
        vista.GrupoSex.clearSelection();
        vista.txtNomBanc.setText(" ");
        vista.txtFechAper.setText(" ");
        vista.txtPorcenRendi.setText(" ");
        vista.txtSaldo.setText(" ");
        vista.txtCant.setText(" ");
        vista.txtNuevSaldo.setText(" ");
    }
    
    public String sex(){
        if(vista.radBtnMasculino.isSelected()){
            return "Masculino";
        }
        else if(vista.radBtnFemenino.isSelected()){
            return "Femenino";
        }
        else{
            return " ";
        }
    }
    public void sexG(){
        if(banc.getCli().getSex() == "Masculino"){
            vista.radBtnMasculino.setSelected(true);
        }
        else if(banc.getCli().getSex() == "Femenino"){
            vista.radBtnFemenino.setSelected(true);
        }
    }
    
    public boolean ValiGuard(){
        if(!vista.txtNumCuenta.getText().isEmpty()
            && !vista.txtNomCli.getText().isEmpty()
            && !vista.txtDom.getText().isEmpty()
            && !vista.txtFechNac.getText().isEmpty()
            && (vista.radBtnMasculino.isSelected() || vista.radBtnFemenino.isSelected())
            && !vista.txtNomBanc.getText().isEmpty()
            && !vista.txtFechAper.getText().isEmpty()
            && !vista.txtPorcenRendi.getText().isEmpty()
            && !vista.txtSaldo.getText().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean ValiCant(){
        if(!vista.txtCant.getText().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    
    public static void main(String[] args) {
        CuentaBancaria banc = new CuentaBancaria();
        dlgBanco vista = new dlgBanco();
        Controlador contr = new Controlador(banc, vista);
        contr.iniciarVista();
    }
    
}